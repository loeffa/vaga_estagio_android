VAGA DE ESTÁGIO DESENVOLVIMENTO ANDROID LOEFFA

Para participar do processo seletivo, clone este projeto para sua máquina, execute-o utilizando Android Studio e envie um email com seu currículo ou LinkedIn para o endereço que será exibido ao executar o aplicativo.

Requisitos:
- Estar cursando CCO ou SIN
- Ter conhecimento prévio do Android Studio
- Familiaridade com versionamento de projetos GIT

Remuneração >= R$ 600,00 (20 hrs) + vale transporte

Local de trabalho: Costeira, Florianópolis.